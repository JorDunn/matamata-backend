from flask import Flask
from matamata.database import db
import os


def create_app():
    app = Flask(__name__)
    if 'MATAMATA_CONFIG' in os.environ:
        app.config.from_envvar('MATAMATA_CONFIG')
    else:
        print('Could not find config file!')

    db.init_app(app)

    from api import api
    app.register_blueprint(api)

    return app
