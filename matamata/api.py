from flask import Blueprint, redirect, request

api = Blueprint('api', __name__)

#
#   API
#
#   All API endpoints require username and password to be passed on all calls.


@api.route('/orders')
def orders(order_status='open'):
    # Returns all orders with a given status, defaulting to open
    pass


@api.route('/order/create')
def order_create():
    # Create an order and return the id.
    pass


@api.route('/order/<order_id>/view')
def order_view(order_id):
    # Fetch and return order information for the given id
    pass


@api.route('/order/<order_id>/update')
def order_update(order_id):
    # Update the order with the given id and return success or error
    pass


@api.route('/order/<order_id>/delete')
def order_delete(order_id):
    # Delete order with the given id
    pass
