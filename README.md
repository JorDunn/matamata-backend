# matamata-backend

[![pipeline status](https://gitlab.com/JorDunn/matamata-backend/badges/master/pipeline.svg)](https://gitlab.com/JorDunn/matamata-backend/commits/master)
[![coverage report](https://gitlab.com/JorDunn/matamata-backend/badges/master/coverage.svg)](https://gitlab.com/JorDunn/matamata-backend/commits/master)

Backend for the [Mata mata](https://en.wikipedia.org/wiki/Mata_mata) order tracking system. This backend serves up the order data via an API.

## TODO

* Sentry support
